import static org.junit.Assert.*;

import java.text.DecimalFormat;

import org.junit.Test;

public class mainTest {

	@Test
	public void testMain() {
		DecimalFormat df = new DecimalFormat("0.00");
		int x;
		
		x = main.main(new String[] {"soup", "soup", "bread"});
		assertTrue(df.format(main.total).equals("1.70"));
		assertTrue(x == 0);
		
		main.main(new String[] {"apples", "apples", "apples"});
		assertTrue(df.format(main.total).equals("2.70"));
		
		x = main.main(new String[] {"soup", "soup", "breadd"});
		assertTrue(x == -1);
		main.main(new String[] {"soup", "soup", "bread"});
		assertTrue(df.format(main.total).equals("1.70"));
		
		main.main(new String[] {});
		assertTrue(df.format(main.total).equals("0.00"));
		
		x = main.main(new String[] {"1", "x", "apples"});
		assertTrue(x == -1);
		
		main.main(new String[] {"soup", "soup", "bread", "soup", "soup", "bread", "apples", "apples", "apples", "bread", "bread"});
		assertTrue(df.format(main.total).equals("7.70"));
	}
}
