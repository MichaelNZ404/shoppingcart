import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class main {
	private static final Map<String, Double> prices;
    static
    {
    	prices = new HashMap<String, Double>();
    	prices.put("soup", .65);
    	prices.put("bread", .80);
        prices.put("milk", 1.30);
        prices.put("apples", 1.00);
    }

    private static Map<String, Integer> basketItems;
    
    static double subtotal;
    static double total;
	public static int main(String[] args) {
		
		subtotal = 0;
		basketItems = new HashMap<String, Integer>();
		 for(int i = 0; i < args.length; i++){
			 
			 String item = args[i].toLowerCase();
			 
			 //We only add items to the basket if we know a price, otherwise we terminate.
			 if(prices.get(item) == null){
				 System.err.println("Cannot find price for: \"" + item + "\" - Terminating");
				 return -1;
			 }
			 
			 //add item to basket
			 Integer count = basketItems.get(args[i]);
			 if(count == null){
				 basketItems.put(item, 1);
			 }else{
				 basketItems.put(item, count +1);
			 }
			 
			 subtotal += prices.get(item);
		 }
		 
		 System.out.println("Subtotal: " + UKprice(subtotal));
		 total = subtotal;
		 
		 //apply discount for apple special
		 Integer appleQuantity = basketItems.get("apples");
		 if(appleQuantity != null){
			 double appleDiscount = appleQuantity * prices.get("apples") * 0.1f;
			 System.out.println("Apples 10% off: -" + UKprice(appleDiscount));
			 total -= appleDiscount;
		 }
		 
		//apply discount for soup/bread special
		 Integer soupQuantity = basketItems.get("soup");
		 Integer breadQuantity = basketItems.get("bread");
		 if(soupQuantity != null && breadQuantity != null){
			 int numberDiscounts = Integer.min(soupQuantity/2, basketItems.get("bread"));
			 double breadDiscount = numberDiscounts * prices.get("bread")/2;
			 System.out.println("Soup & bread combo: -" + UKprice(breadDiscount));
			 total -= breadDiscount;
		 }
		 
		 if(total == subtotal){
			 System.out.println("(No�offers�available)");
		 }
		 
		 System.out.println("Total: " + UKprice(total));
		 return 0;
		 
	}
	
	public static String UKprice(double price){
		DecimalFormat df = new DecimalFormat("0.00");
		
		if(price > 1){
			return "�" + df.format(price);
		}
		else{
			return df.format(price) + "p";
		}
	}
}
